# Belarusian translation for ubuntu-ui-extras
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd 2016
# This file is distributed under the same license as the ubuntu-ui-extras package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-ui-extras\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-04-04 12:56+0200\n"
"PO-Revision-Date: 2020-12-26 13:26+0000\n"
"Last-Translator: Zmicer <nashtlumach@gmail.com>\n"
"Language-Team: Belarusian <https://translate.ubports.com/projects/ubports/"
"ubuntu-ui-extras/be/>\n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<="
"4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2017-03-19 06:38+0000\n"

#: modules/Ubuntu/Components/Extras/Example/Printers.qml:106
msgid "Aborted"
msgstr "Перарвана"

#: modules/Ubuntu/Components/Extras/Example/Printers.qml:108
msgid "Active"
msgstr "Актыўны"

#: modules/Ubuntu/Components/Extras/PhotoEditor.qml:206
#: modules/Ubuntu/Components/Extras/PhotoEditor/ExposureAdjuster.qml:81
msgid "Cancel"
msgstr "Скасаваць"

#: modules/Ubuntu/Components/Extras/Printers/backend/backend_pdf.cpp:44
msgid "Color"
msgstr "Колер"

#: modules/Ubuntu/Components/Extras/Printers/models/printermodel.cpp:54
msgid "Create PDF"
msgstr "Стварыць PDF"

#: modules/Ubuntu/Components/Extras/PhotoEditor.qml:43
#: modules/Ubuntu/Components/Extras/PhotoEditor/CropOverlay.qml:349
msgid "Crop"
msgstr "Абрэзаць"

#: modules/Ubuntu/Components/Extras/PhotoEditor/ExposureAdjuster.qml:72
msgid "Done"
msgstr "Завершана"

#: modules/Ubuntu/Components/Extras/PhotoEditor.qml:226
msgid "Enhancing photo..."
msgstr "Паляпшэнне фотаздымка…"

#: modules/Ubuntu/Components/Extras/Example/Printers.qml:104
msgid "Idle"
msgstr "Бяздзейнічае"

#: modules/Ubuntu/Components/Extras/Printers/utils.h:61
msgid "Long Edge (Standard)"
msgstr "Доўгі край (стандарт)"

#: modules/Ubuntu/Components/Extras/Example/Printers.qml:115
msgid "No messages"
msgstr "Няма паведамленняў"

#: modules/Ubuntu/Components/Extras/Printers/backend/backend_pdf.cpp:47
msgid "Normal"
msgstr "Звычайны"

#: modules/Ubuntu/Components/Extras/Printers/utils.h:64
msgid "One Sided"
msgstr "Аднабаковы"

#: modules/Ubuntu/Components/Extras/PhotoEditor/EditStack.qml:121
msgid "Redo"
msgstr "Паўтарыць"

#: modules/Ubuntu/Components/Extras/PhotoEditor.qml:212
msgid "Revert Photo"
msgstr "Аднавіць фотаздымак"

#: modules/Ubuntu/Components/Extras/PhotoEditor/EditStack.qml:128
#: modules/Ubuntu/Components/Extras/PhotoEditor.qml:196
msgid "Revert to original"
msgstr "Вярнуцца да арыгінала"

#: modules/Ubuntu/Components/Extras/PhotoEditor.qml:52
msgid "Rotate"
msgstr "Павярнуць"

#: modules/Ubuntu/Components/Extras/Printers/utils.h:59
msgid "Short Edge (Flip)"
msgstr "Кароткі край (гартанне)"

#: modules/Ubuntu/Components/Extras/Example/Printers.qml:110
msgid "Stopped"
msgstr "Спынены"

#: modules/Ubuntu/Components/Extras/Printers/printers/printers.cpp:395
msgid "Test page"
msgstr "Тэставая старонка"

#: modules/Ubuntu/Components/Extras/PhotoEditor.qml:197
msgid "This will undo all edits, including those from previous sessions."
msgstr "Гэта скасуе ўсе змены, уключаючы змены з папярэдніх сеансаў."

#: modules/Ubuntu/Components/Extras/PhotoEditor/EditStack.qml:114
msgid "Undo"
msgstr "Адрабіць"
